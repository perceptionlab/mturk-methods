# README #

This is the parent repository for Carleton Perception Lab's psiturk instance on psiturk-perception.rhcloud.com that hosts experiments for the Amazon Mechanical Turk platform. Pull from here into the app-root/repo directory to update the live instace.